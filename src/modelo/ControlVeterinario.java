package modelo;

public class ControlVeterinario {

    public int idVisita;
    public int idMascota;
    public String fecha;
    public String razonVisita;
    public String tratamiento;
    public int costo;


    public ControlVeterinario(int idVisita, int idMascota, String fecha, String razonVisita, String tratamiento, int costo) {
        this.idVisita = idVisita;
        this.idMascota = idMascota;
        this.fecha = fecha;
        this.razonVisita = razonVisita;
        this.tratamiento = tratamiento;
        this.costo = costo;
    }

    public ControlVeterinario() {

    }


    public int getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(int idVisita) {
        this.idVisita = idVisita;
    }

    public int getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(int idMascota) {
        this.idMascota = idMascota;
    }

    public String getFecha() {

        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getRazonVisita() {
        return razonVisita;
    }

    public void setRazonVisita(String razonVisita) {
        this.razonVisita = razonVisita;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    @Override
    public String toString() {
        return "ControlVeterinario{" +
                "idVisita=" + idVisita +
                ", idMascota=" + idMascota +
                ", fecha=" + fecha +
                ", razonVisita='" + razonVisita + '\'' +
                ", tratamiento='" + tratamiento + '\'' +
                ", costo=" + costo +
                '}';
    }
}
