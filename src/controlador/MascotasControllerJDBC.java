package controlador;

import modelo.Duenios;
import modelo.Mascotas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MascotasControllerJDBC implements MascotasDAO{

    Connection con;
    public final String SELECT_ALL = "SELECT * FROM mascotas";
    public final String SELECTBYID = "SELECT * FROM mascotas WHERE idMascota = ?";
    public final String COUNTESPECIE = "SELECT COUNT(especie) AS 'CantidadEspecies' FROM mascotas";
    public final String INSERTMASCOTAS = "INSERT mascotas (Nombre,Especie,Raza,Color,Edad,idDuenio) VALUES (?,?,?,?,?,?)";
    public final String DELETEMASCOTAID = "DELETE FROM mascotas WHERE idMascota= ?";
    public final String UPDATEALL = "UPDATE mascotas SET nombre=?,especie=?,raza=?,color=?,edad=?, idDuenio=? WHERE idMascota=?";
    public final String UPDATERAZABYID = "UPDATE mascotas SET Raza = ? WHERE idMascota = ?";

    public MascotasControllerJDBC(Connection con) {
        this.con = con;
    }


    @Override
    public ArrayList<Mascotas> selectAll() {
        ArrayList<Mascotas> lista = new ArrayList<>();
        try {
            PreparedStatement statement = con.prepareStatement(SELECT_ALL);
            ResultSet rs = statement.executeQuery();

            while (rs.next()){
                int mascotaId = rs.getInt("IdMascota");
                String nombreMascota = rs.getString("Nombre");
                String especieMascota = rs.getString("Especie");
                String razaMascota= rs.getString("Raza");
                String colorMascota = rs.getString("Color");
                String edadMascota = rs.getString("Edad");
                int duenioMascotaid = rs.getInt("idDuenio");


               Mascotas mascota1 = new Mascotas(mascotaId, nombreMascota, especieMascota, razaMascota, colorMascota, edadMascota, duenioMascotaid);
                lista.add(mascota1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista;
    }

    @Override
    public Mascotas selectById(int idMascota) {
        Mascotas mascota = null; // se pone null porque si no encuentra ningun resultado me devuelve null, pero si encuentra un resultado , se crea una nueva instancia de la clase Duenios y se le asignara esa instancia a la variable, remplazando ese null.

        try {
            PreparedStatement statement = con.prepareStatement(SELECTBYID);
            statement.setInt(1,idMascota);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                mascota = new Mascotas();
                mascota.setIdMascota(rs.getInt("IdMascota"));
                mascota.setNombre(rs.getString("Nombre"));
                mascota.setEspecie(rs.getString("Especie"));
                mascota.setRaza(rs.getString("Raza"));
                mascota.setColor(rs.getString("Color"));
                mascota.setEdad(rs.getString("Edad"));
                mascota.setIdDuenio(rs.getInt("IdDuenio"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return mascota;
    }

    @Override
    public int cantidadEspecies() {
        int resultado= 0;
        try{
            PreparedStatement statement = con.prepareStatement(COUNTESPECIE);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                resultado =  rs.getInt("CantidadEspecies");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return resultado;
    }

    @Override
    public Mascotas insertMascotas(Mascotas mascotas) {
        try{
            PreparedStatement statement = con.prepareStatement(INSERTMASCOTAS );
            statement.setString(1, mascotas.getNombre());
            statement.setString(2, mascotas.getEspecie());
            statement.setString(3, mascotas.getRaza());
            statement.setString(4, mascotas.getColor());
            statement.setString(5, mascotas.getEdad());
            statement.setInt(6, mascotas.getIdDuenio());


            int filasAfectadas = statement.executeUpdate(); //verifico que se haya ingresado los nuevos datos
            if (filasAfectadas > 0) {
                System.out.println("Los datos fueron ingresados correctamente.");
                return mascotas;
            } else {
                System.out.println("No se pudo insertar el registro.");
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public boolean deleteMascotaId(int idMascota) {
        try{
            PreparedStatement statement = con.prepareStatement(DELETEMASCOTAID );
            statement.setInt(1, idMascota);

            int filaBorrada = statement.executeUpdate();  //ejecuto la consulta y modifico
            return  filaBorrada > 0; //devuelve true si se elimino al menos 1 fila
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean updateAll(int idMascota, String nombre, String especie, String raza, String color, String edad, int idDuenio) {
        boolean actualizado = false;
        try{
            PreparedStatement statement = con.prepareStatement(UPDATEALL);
            statement.setString(1, nombre);
            statement.setString(2, especie);
            statement.setString(3, raza);
            statement.setString(4, color);
            statement.setString(5, edad);
            statement.setInt(6, idDuenio);
            statement.setInt(7, idMascota);


            int filaBorrada = statement.executeUpdate();
            if (filaBorrada > 0){
                System.out.println("Se ha actualizado correctamente");
                actualizado = true;
                return actualizado;
            }else {
                System.out.println("No se ha actualizado");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return actualizado;
    }

    @Override
    public boolean updateEspecieById(int idMascota, String especie) {

        try {
            PreparedStatement statement = con.prepareStatement(UPDATERAZABYID);
            statement.setString(1,especie);
            statement.setInt(2,idMascota);

            int filaBorrada = statement.executeUpdate();
            if (filaBorrada > 0){
                System.out.println("Se ha actualizado la especie correctamente ");
            }else {
                System.out.println("No se ha actulizado");
            }
            return filaBorrada > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
