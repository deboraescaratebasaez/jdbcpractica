package controlador;

import modelo.Duenios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DueniosControllerJDBC implements DueniosDAO {

    Connection con;
    public final String SELECT_ALL = "SELECT * FROM duenios";
    public final String SELECTBYID = "SELECT * FROM duenios WHERE idDuenio = ?";
    public final String COUNTID = "SELECT COUNT(idDuenio) AS 'NumeroTotalDeDuenios' FROM duenios";
    public final String INSERTDUENIOS = "INSERT duenios (Nombre,Apellido,CorreoElectronico,Telefono,Direccion,Ciudad) VALUES (?,?,?,?,?,?)";
    public final String DELETEDUENIOSID = "DELETE FROM duenios WHERE idDuenio = ?";
    public final String UPDATEALL = "UPDATE duenios SET nombre=?,apellido=?,correoElectronico=?,telefono=?,direccion=?,ciudad=? WHERE idDuenio=?";
    public final String UPDATEDIRRECCIONBYID= "UPDATE duenios SET Direccion = ? WHERE idDuenio = ?";

    public DueniosControllerJDBC(Connection con) {
        this.con = con;
    }


    @Override
    public ArrayList<Duenios> selectAll() {
        ArrayList<Duenios> lista = new ArrayList<>();
        try {
            PreparedStatement statement = con.prepareStatement(SELECT_ALL);
            ResultSet rs = statement.executeQuery();

            while (rs.next()){
                int duenioId = rs.getInt("IdDuenio");
                String nombreDuenio = rs.getString("Nombre");
                String apellidoDuenio = rs.getString("Apellido");
                String correoDuenio = rs.getString("CorreoElectronico");
                String telefonoDuenio = rs.getString("telefono");
                String direccionDuenio = rs.getString("Direccion");
                String ciudadDuenio = rs.getString("Ciudad");


                Duenios duenio1 = new Duenios(duenioId, nombreDuenio, apellidoDuenio, correoDuenio, telefonoDuenio, direccionDuenio, ciudadDuenio);
                lista.add(duenio1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista;
    }

    @Override
    public Duenios selectById(int idDuenios) {
        Duenios duenio = null; // se pone null porque si no encuentra ningun resultado me devuelve null, pero si encuentra un resultado , se crea una nueva instancia de la clase Duenios y se le asignara esa instancia a la variable, remplazando ese null.

        try {
            PreparedStatement statement = con.prepareStatement(SELECTBYID);
            statement.setInt(1,idDuenios);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                duenio = new Duenios();
                duenio.setIdDuenios(rs.getInt("IdDuenio"));
                duenio.setNombre(rs.getString("Nombre"));
                duenio.setApellido(rs.getString("Apellido"));
                duenio.setCorreoElectronico(rs.getString("CorreoElectronico"));
                duenio.setTelefono(rs.getString("Telefono"));
                duenio.setDireccion(rs.getString("Direccion"));
                duenio.setCiudad(rs.getString("Ciudad"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return duenio;
    }

    @Override
    public int cantidadDuenos() {
        int resultado= 0;
        try{
           PreparedStatement statement = con.prepareStatement(COUNTID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
             resultado =  rs.getInt("NumeroTotalDeDuenios");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return resultado;
    }


    @Override
    public Duenios insertDuenios(Duenios duenio) {


        try{
            PreparedStatement statement = con.prepareStatement(INSERTDUENIOS);
            statement.setString(1, duenio.getNombre());
            statement.setString(2,duenio.getApellido());
            statement.setString(3, duenio.getCorreoElectronico());
            statement.setString(4, duenio.getTelefono());
            statement.setString(5,duenio.getDireccion());
            statement.setString(6,duenio.getCiudad());


            int filasAfectadas = statement.executeUpdate(); //verifico que se haya ingresado los nuevos datos
            if (filasAfectadas > 0) {
                System.out.println("Los datos fueron ingresados correctamente.");
                return duenio; // Retornamos el objeto Duenios si se ingresaron correctamente
            } else {
                System.out.println("No se pudo insertar el registro.");
            }


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public boolean deleteDueniosId(int id) {

        try{
            PreparedStatement statement = con.prepareStatement(DELETEDUENIOSID);
            statement.setInt(1, id);

            int filaBorrada = statement.executeUpdate();  //ejecuto la consulta y modifico
            return  filaBorrada > 0; //devuelve true si se elimino al menos 1 fila
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean updateAll(int idDuenio, String nombre, String apellido, String CorreoElectronico, String Telefono, String Direccion, String Ciudad) {

        boolean actualizado = false;
        try{
            PreparedStatement statement = con.prepareStatement(UPDATEALL);
            statement.setString(1, nombre);
            statement.setString(2, apellido);
            statement.setString(3, CorreoElectronico);
            statement.setString(4, Telefono);
            statement.setString(5, Direccion);
            statement.setString(6, Ciudad);
            statement.setInt(7,idDuenio);


            int filaBorrada = statement.executeUpdate();
            if (filaBorrada > 0){
                System.out.println("Se ha actualizado correctamente");
              actualizado = true;
              return actualizado;
            }else {
                System.out.println("No se ha actualizado");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return actualizado;
    }


    @Override
    public boolean updateDireccionById(int idDuenio, String direccion) {

        try {
            PreparedStatement statement = con.prepareStatement(UPDATEDIRRECCIONBYID);
            statement.setString(1,direccion);
            statement.setInt(2,idDuenio);

            int filaBorrada = statement.executeUpdate();
            if (filaBorrada > 0){
                System.out.println("Se ha actualizado la direccion correctamente ");
            }else {
                System.out.println("No se ha actulizado");
            }
            return filaBorrada > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
