package controlador;

import modelo.ControlVeterinario;
import modelo.Mascotas;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

public interface ControlVeterinarioDAO {

    //LOS METODOS QUE TIENE QUE TENER EL CONTROLADOR CONTROLVETERINARIO

    //A. mostrar todos los datos de una tabla a elección.
    ArrayList<ControlVeterinario> selectAll();

    //B. mostrar todos los datos de una tabla según una condición (buscar por id, por nombre por ejemplo) de una tabla a elección.
    ControlVeterinario selectById(int idVisita);

    //c. mostrar usando una función de agrupación (max, min, avg, sum...) de una tabla a elección
    int mayorCosto();

    //d. insertar datos de una tabla a elección.
    ControlVeterinario insertControl (ControlVeterinario controlVeterinario);

    //e. borrar datos por clave primaria de una tabla a eleccion
    boolean deleteControlVeterinarioId (int idVisita);

    //f. actualizar todos los parámetros de una tabla a elección.
    boolean updateAll(int idVisita, String fecha, String RazonVisita, String Tratamiento, int Costo);

    //g. Actualizar un parámetro de una tabla a elección.
    boolean updateTratamientoById (int idVisita, String Tratamiento);
}

