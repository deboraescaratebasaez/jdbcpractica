package controlador;

import modelo.ControlVeterinario;
import modelo.Duenios;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;


public class ControlVeterinarioControllerJDBC implements ControlVeterinarioDAO{


    Connection con;
    public final String SELECT_ALL = "SELECT * FROM controlveterinario";
    public final String SELECTBYID = "SELECT * FROM controlveterinario WHERE idVisita = ?";
    public final String MAX = "SELECT MAX(costo) AS 'MayorCosto' from controlveterinario;";
    public final String INSERTCONTROL = "INSERT controlveterinario (IdMascota,Fecha,RazonVisita,Tratamiento,Costo) VALUES (?,?,?,?,?)";
    public final String DELETECONTROLVETERTINARIOID = "DELETE FROM controlveterinario WHERE idVisita = ?";
    public final String UPDATEALL = "UPDATE controlveterinario SET fecha=?,razonVisita=?,tratamiento=?,costo=? WHERE idVisita=?";
    public final String UPDATETRATAMIENTOBYID = "UPDATE controlveterinario SET Tratamiento = ? WHERE idVisita= ?";

    public ControlVeterinarioControllerJDBC(Connection con) {
        this.con = con;
    }


    @Override
    public ArrayList<ControlVeterinario> selectAll() {
        ArrayList<ControlVeterinario> lista = new ArrayList<>();
        try {
            PreparedStatement statement = con.prepareStatement(SELECT_ALL);
            ResultSet rs = statement.executeQuery();

            while (rs.next()){
                int VisitaId = rs.getInt("IdVisita");
                int mascotaId = rs.getInt("IdMascota");
                String fechaControl = rs.getString("Fecha");
                String razonVisitaControl = rs.getString("razonVisita");
                String tratamientoControl = rs.getString("tratamiento");
                int costoControl = rs.getInt("Costo");

                ControlVeterinario controlVeterinario1 = new ControlVeterinario(VisitaId, mascotaId, fechaControl, razonVisitaControl, tratamientoControl, costoControl);
                lista.add(controlVeterinario1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista;
    }

    @Override
    public ControlVeterinario selectById(int idVisita) {

        ControlVeterinario controlVeterinario = null; // se pone null porque si no encuentra ningun resultado me devuelve null, pero si encuentra un resultado , se crea una nueva instancia de la clase Duenios y se le asignara esa instancia a la variable, remplazando ese null.

        try {
            PreparedStatement statement = con.prepareStatement(SELECTBYID);
            statement.setInt(1,idVisita);
            ResultSet rs = statement.executeQuery();

            if(rs.next()){
                controlVeterinario = new ControlVeterinario();
                controlVeterinario.setIdVisita(rs.getInt("IdVisita"));
                controlVeterinario.setIdMascota(rs.getInt("IdMascota"));
                controlVeterinario.setFecha(rs.getString("Fecha"));
                controlVeterinario.setRazonVisita(rs.getString("RazonVisita"));
                controlVeterinario.setTratamiento(rs.getString("Tratamiento"));
                controlVeterinario.setCosto(rs.getInt("Costo"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return controlVeterinario;
    }

    @Override
    public int mayorCosto() {
        int resultado= 0;
        try{
            PreparedStatement statement = con.prepareStatement(MAX);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                resultado =  rs.getInt("MayorCosto");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return resultado;
    }

    @Override
    public ControlVeterinario insertControl(ControlVeterinario controlVeterinario) {

        try{
            PreparedStatement statement = con.prepareStatement(INSERTCONTROL );
            statement.setInt(1, controlVeterinario.getIdMascota());
            statement.setString(2,controlVeterinario.getFecha());
            statement.setString(3, controlVeterinario.getRazonVisita());
            statement.setString(4, controlVeterinario.getTratamiento());
            statement.setInt(5,controlVeterinario.getCosto());

            int filasAfectadas = statement.executeUpdate(); //verifico que se haya ingresado los nuevos datos
            if (filasAfectadas > 0) {
                System.out.println("Los datos fueron ingresados correctamente.");
                return controlVeterinario; // Retornamos el objeto Duenios si se ingresaron correctamente
            } else {
                System.out.println("No se pudo insertar el registro.");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return null;
    }

    @Override
    public boolean deleteControlVeterinarioId(int idVisita) {

        try{
            PreparedStatement statement = con.prepareStatement(DELETECONTROLVETERTINARIOID );
            statement.setInt(1, idVisita);

            int filaBorrada = statement.executeUpdate();  //ejecuto la consulta y modifico
            return  filaBorrada > 0; //devuelve true si se elimino al menos 1 fila
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean updateAll(int idVisita, String fecha, String RazonVisita, String Tratamiento, int Costo) {
        boolean actualizado = false;
        try{
            PreparedStatement statement = con.prepareStatement(UPDATEALL);
            statement.setString(1, fecha);
            statement.setString(2, RazonVisita);
            statement.setString(3, Tratamiento);
            statement.setInt(4, Costo);
            statement.setInt(5,idVisita);


            int filaBorrada = statement.executeUpdate();
            if (filaBorrada > 0){
                System.out.println("Se ha actualizado correctamente");
                actualizado = true;
                return actualizado;
            }else {
                System.out.println("No se ha actualizado");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return actualizado;
    }

    @Override
    public boolean updateTratamientoById(int idVisita, String Tratamiento) {

        try {
            PreparedStatement statement = con.prepareStatement(UPDATETRATAMIENTOBYID);
            statement.setString(1,Tratamiento);
            statement.setInt(2,idVisita);

            int filaBorrada = statement.executeUpdate();
            if (filaBorrada > 0){
                System.out.println("Se ha actualizado el tratamiento correctamente ");
            }else {
                System.out.println("No se ha actualizado");
            }
            return filaBorrada > 0;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
