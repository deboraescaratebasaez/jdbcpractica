package controlador;

import modelo.Duenios;
import modelo.Mascotas;

import java.util.ArrayList;

public interface MascotasDAO {

    //LOS METODOS QUE TIENE QUE TENER EL CONTROLADOR MASCOTAS

    //A. mostrar todos los datos de una tabla a elección.
    ArrayList<Mascotas> selectAll();

    //B. mostrar todos los datos de una tabla según una condición (buscar por id, por nombre por ejemplo) de una tabla a elección.
    Mascotas selectById(int idMascota);


    //c. mostrar usando una función de agrupación (max, min, avg, sum...) de una tabla a elección
    int cantidadEspecies();

    //d. insertar datos de una tabla a elección.
   Mascotas insertMascotas (Mascotas mascotas);

    //e. borrar datos por clave primaria de una tabla a eleccion
    boolean deleteMascotaId (int idMascota);

    //f. actualizar todos los parámetros de una tabla a elección.
    boolean updateAll(int idMascota, String Nombre, String Especie, String Raza, String Color, String Edad, int IdDuenio);

    //g. Actualizar un parámetro de una tabla a elección.
    boolean updateEspecieById (int id, String Especie);
}
