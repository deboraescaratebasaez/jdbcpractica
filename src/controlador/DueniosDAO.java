package controlador;

import modelo.Duenios;

import java.util.ArrayList;

public interface DueniosDAO {

    //LOS METODOS QUE TIENE QUE TENER EL CONTROLADOR DUENIOS

    //A. mostrar todos los datos de una tabla a elección.
    ArrayList<Duenios> selectAll();

    //B. mostrar todos los datos de una tabla según una condición (buscar por id, por nombre por ejemplo) de una tabla a elección.
    Duenios selectById(int idDuenios);


    //c. mostrar usando una función de agrupación (max, min, avg, sum...) de una tabla a elección
    int cantidadDuenos();

    //d. insertar datos de una tabla a elección.
    Duenios insertDuenios (Duenios duenios);

    //e. borrar datos por clave primaria de una tabla a eleccion
    boolean deleteDueniosId (int id);

    //f. actualizar todos los parámetros de una tabla a elección.
    boolean updateAll(int idDuenio, String nombre, String apellido, String CorreoElectronico, String Telefono, String Direccion, String Ciudad);

    //g. Actualizar un parámetro de una tabla a elección.
    boolean updateDireccionById (int id, String direccion);

}
