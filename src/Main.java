import controlador.ControlVeterinarioControllerJDBC;
import controlador.DueniosControllerJDBC;
import controlador.MascotasControllerJDBC;
import modelo.ControlVeterinario;
import modelo.Duenios;
import modelo.Mascotas;
import utilidades.Conexion;

import java.sql.Connection;
import java.sql.SQLOutput;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Conexion conexion = new Conexion();
        Connection con = conexion.getConnection();

        Scanner sc = new Scanner(System.in);


        mostrarMenu();
        int seleccionTabla = sc.nextInt();

        switch (seleccionTabla) {
            case 1:
                DueniosControllerJDBC dueniosTabla = new DueniosControllerJDBC(con);
                menuOperaciones();
                int opcionOperacion = sc.nextInt();
                switch (opcionOperacion) {
                    case 1:
                        List<Duenios> lista = dueniosTabla.selectAll();
                        System.out.println(lista);

                        break;
                    case 2:
                        System.out.println("Ingresa el ID que quieres visualizar");
                        int opcionID = sc.nextInt();
                        Duenios duenio = dueniosTabla.selectById(opcionID);
                        System.out.println(duenio);
                        break;
                    case 3:
                        DueniosControllerJDBC countMadrid = new DueniosControllerJDBC(con);
                        int resultadoDuenio = countMadrid.cantidadDuenos();
                        System.out.println("La cantidad de dueños es: " + resultadoDuenio);
                        break;
                    case 4:
                        System.out.println("Ingrese el nombre:");
                        sc.nextLine(); // Consumir la nueva línea pendiente
                        String nombre = sc.nextLine();
                        System.out.println("Ingrese el apellido:");
                        String apellido = sc.nextLine();
                        System.out.println("Ingrese el correo electronico: ");
                        String email = sc.nextLine();
                        System.out.println("Ingrese el Teléfono: ");
                        String telefono = sc.nextLine();
                        System.out.println("Ingrese el dirección: ");
                        String direccion = sc.nextLine();
                        System.out.println("Ingrese el ciudad: ");
                        String ciudad = sc.nextLine();
                        Duenios nuevoDuenio = new Duenios();
                        nuevoDuenio.setNombre(nombre);
                        nuevoDuenio.setApellido(apellido);
                        nuevoDuenio.setCorreoElectronico(email);
                        nuevoDuenio.setTelefono(telefono);
                        nuevoDuenio.setDireccion(direccion);
                        nuevoDuenio.setCiudad(ciudad);

                        DueniosControllerJDBC duenioNuevo = new DueniosControllerJDBC(con);
                        Duenios resultado = duenioNuevo.insertDuenios(nuevoDuenio);
                        break;
                    case 5:
                        System.out.println("Ingrese el ID que desea borrar: ");
                        int opcionBorrar = sc.nextInt();
                        DueniosControllerJDBC duenioBorrado = new DueniosControllerJDBC(con);
                        duenioBorrado.deleteDueniosId(opcionBorrar);
                        break;
                    case 6:
                        System.out.println("Ingrese el ID del registro que desea actualizar: ");
                        int idActualizar = sc.nextInt();
                        System.out.println("Ingrese el nuevo nombre:");
                        sc.nextLine(); // Consumir la nueva línea pendiente
                        String nombreActualizado = sc.nextLine();
                        System.out.println("Ingrese el nuevo apellido:");
                        String apellidoActualizado = sc.nextLine();
                        System.out.println("Ingrese el nuevo correo electronico: ");
                        String emailActualizado = sc.nextLine();
                        System.out.println("Ingrese el nuevo Teléfono: ");
                        String telefonoActualizado = sc.nextLine();
                        System.out.println("Ingrese la nuevo dirección: ");
                        String direccionActualizado = sc.nextLine();
                        System.out.println("Ingrese la nuevo ciudad: ");
                        String ciudadActualizado = sc.nextLine();
                        Duenios actualizadoDuienio = new Duenios();
                        actualizadoDuienio.setNombre(nombreActualizado);
                        actualizadoDuienio.setApellido(apellidoActualizado);
                        actualizadoDuienio.setCorreoElectronico(emailActualizado);
                        actualizadoDuienio.setTelefono(telefonoActualizado);
                        actualizadoDuienio.setDireccion(direccionActualizado);
                        actualizadoDuienio.setCiudad(ciudadActualizado);

                        DueniosControllerJDBC duenioActualizado = new DueniosControllerJDBC(con);
                        boolean resultado2 = duenioActualizado.updateAll(idActualizar, nombreActualizado, apellidoActualizado, emailActualizado, telefonoActualizado, direccionActualizado, ciudadActualizado);
                        break;
                    case 7:
                        System.out.println("Ingrese el ID del registro que desea actualizar: ");
                        int idActualizar2 = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Ingrese la nuevo dirección: ");
                        String direccionActualizado2 = sc.nextLine();

                        Duenios actualizaDireccion = new Duenios();
                        actualizaDireccion.setDireccion(direccionActualizado2);

                        DueniosControllerJDBC actualizacionDireccion = new DueniosControllerJDBC(con);
                        boolean resultado3 = actualizacionDireccion.updateDireccionById(idActualizar2, direccionActualizado2);
                        break;
                }

            case 2:
                MascotasControllerJDBC mascotaTabla = new MascotasControllerJDBC(con);
                menuOperaciones();
                int opcionOperacion2 = sc.nextInt();
                switch (opcionOperacion2) {
                    case 1:
                        List<Mascotas> lista = mascotaTabla.selectAll();
                        System.out.println(lista);
                        break;
                    case 2:
                        System.out.println("Ingresa el ID que quieres visualizar");
                        int opcionID = sc.nextInt();
                        Mascotas mascota = mascotaTabla.selectById(opcionID);
                        System.out.println(mascota);
                        break;
                    case 3:
                        MascotasControllerJDBC countEspecies = new MascotasControllerJDBC(con);
                        int resultadoMascota = countEspecies.cantidadEspecies();
                        System.out.println("La cantidad de mascotas es: " + resultadoMascota);
                        break;
                    case 4:
                        System.out.println("Ingrese el nombre:");
                        sc.nextLine(); // Consumir la nueva línea pendiente
                        String nombre = sc.nextLine();
                        System.out.println("Ingrese la especie:");
                        String especie = sc.nextLine();
                        System.out.println("Ingrese la raza: ");
                        String raza = sc.nextLine();
                        System.out.println("Ingrese el color: ");
                        String color = sc.nextLine();
                        System.out.println("Ingrese la edad: ");
                        String edad = sc.nextLine();
                        System.out.println("Ingrese el idDuenio ");
                        int idDuenio = sc.nextInt();
                        Mascotas nuevoMascota = new Mascotas();
                        nuevoMascota.setNombre(nombre);
                        nuevoMascota.setEspecie(especie);
                        nuevoMascota.setRaza(raza);
                        nuevoMascota.setColor(color);
                        nuevoMascota.setEdad(edad);
                        nuevoMascota.setIdDuenio(idDuenio);

                        MascotasControllerJDBC mascotaNuevo = new MascotasControllerJDBC(con);
                        Mascotas resultado = mascotaNuevo.insertMascotas(nuevoMascota);
                        break;
                    case 5:
                        System.out.println("Ingrese el ID que desea borrar: ");
                        int opcionBorrar = sc.nextInt();
                        MascotasControllerJDBC mascotaBorrado = new MascotasControllerJDBC(con);
                        mascotaBorrado.deleteMascotaId(opcionBorrar);
                        break;
                    case 6:
                        System.out.println("Ingrese el ID del registro que desea actualizar: ");
                        int idActualizar = sc.nextInt();
                        System.out.println("Ingrese el nuevo Nombre de la mascota:");
                        sc.nextLine(); // Consumir la nueva línea pendiente
                        String nombreActualizado = sc.nextLine();
                        System.out.println("Ingrese la nueva Especie:");
                        String especieActualizado = sc.nextLine();
                        System.out.println("Ingrese la nueva Raza de la mascota: ");
                        String razaActualizado = sc.nextLine();
                        System.out.println("Ingrese el nuevo Color de la mascota: ");
                        String colorActualizado = sc.nextLine();
                        System.out.println("Ingrese la nueva Edad de la mascota: ");
                        String edadActualizado = sc.nextLine();
                        System.out.println("Ingrese el id del dueño de la mascota: ");
                        int idDuenioActualizado = sc.nextInt();
                        Mascotas actualizadoMascotas = new Mascotas();
                        actualizadoMascotas.setNombre(nombreActualizado);
                        actualizadoMascotas.setEspecie(especieActualizado);
                        actualizadoMascotas.setRaza(razaActualizado);
                        actualizadoMascotas.setColor(colorActualizado);
                        actualizadoMascotas.setEdad(edadActualizado);
                        actualizadoMascotas.setIdDuenio(idDuenioActualizado);

                        MascotasControllerJDBC mascotasActualizado = new MascotasControllerJDBC(con);
                        boolean resultado2 = mascotasActualizado.updateAll(idActualizar, nombreActualizado, especieActualizado, razaActualizado, colorActualizado, edadActualizado, idDuenioActualizado);
                        break;
                    case 7:
                        System.out.println("Ingrese el ID del registro que desea actualizar: ");
                        int idActualizar2 = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Ingrese la nuevo especie: ");
                        String especieActualizado2 = sc.nextLine();

                        Mascotas actualizaEspecie = new Mascotas();
                        actualizaEspecie.setEspecie(especieActualizado2);

                        MascotasControllerJDBC actualizacionEspecie = new MascotasControllerJDBC(con);
                        boolean resultado3 = actualizacionEspecie.updateEspecieById(idActualizar2, especieActualizado2);
                        break;
                }

            case 3:
                ControlVeterinarioControllerJDBC controlVeterinarioTabla = new ControlVeterinarioControllerJDBC(con);
                menuOperaciones();
                int opcionOperacion3 = sc.nextInt();
                switch (opcionOperacion3) {
                    case 1:
                        List<ControlVeterinario> lista = controlVeterinarioTabla.selectAll();
                        System.out.println(lista);
                        break;
                    case 2:
                        System.out.println("Ingresa el ID que deseas visualizar");
                        int opcionID = sc.nextInt();
                        ControlVeterinario controlVeterinario = controlVeterinarioTabla.selectById(opcionID);
                        System.out.println(controlVeterinario);
                        break;
                    case 3:
                        ControlVeterinarioControllerJDBC maxCosto = new ControlVeterinarioControllerJDBC(con);
                        int resultadoMax = maxCosto.mayorCosto();
                        System.out.println("El costo máximo ha sido de : " + resultadoMax);
                        break;
                    case 4:
                        System.out.println("Ingrese el Id de la Mascota:");
                        sc.nextLine(); // Consumir la nueva línea pendiente
                        int idMascota = sc.nextInt();
                        System.out.println("Ingrese la fecha (formato YYYY-MM-DD):");
                        sc.nextLine(); // Consumir la nueva línea pendiente
                        String fecha = sc.nextLine();
                        System.out.println("Ingrese la razón de la visita: ");
                        String razonVisita = sc.nextLine();
                        System.out.println("Ingrese el tratamiento indicado: ");
                        String tratamiento = sc.nextLine();
                        System.out.println("Ingrese el costo de la consulta: ");
                        int costo = sc.nextInt();

                        ControlVeterinario nuevoControl = new ControlVeterinario();
                        nuevoControl.setIdMascota(idMascota);
                        nuevoControl.setFecha(fecha);
                        nuevoControl.setRazonVisita(razonVisita);
                        nuevoControl.setTratamiento(tratamiento);
                        nuevoControl.setCosto(costo);

                        ControlVeterinarioControllerJDBC controlNuevo = new ControlVeterinarioControllerJDBC(con);
                        ControlVeterinario resultado = controlNuevo.insertControl(nuevoControl);
                        break;
                    case 5:
                        System.out.println("Ingrese el ID que desea borrar: ");
                        int opcionBorrar = sc.nextInt();
                        ControlVeterinarioControllerJDBC controlBorrado = new ControlVeterinarioControllerJDBC(con);
                        controlBorrado.deleteControlVeterinarioId(opcionBorrar);
                        break;
                    case 6:
                        System.out.println("Ingrese el ID del registro que desea actualizar: ");
                        int idActualizar = sc.nextInt();
                        System.out.println("Ingrese la nueva fecha (formato YYYY-MM-DD):");
                        sc.nextLine(); // Consumir la nueva línea pendiente3
                        String fechaActualizada = sc.nextLine();
                        System.out.println("Ingrese la razón de la visita: ");
                        String razonActualizado = sc.nextLine();
                        System.out.println("Ingrese el tratamiento indicado: ");
                        String tratamientoActualizado = sc.nextLine();
                        System.out.println("Ingrese el costo: ");
                        int costoActualizado = sc.nextInt();

                        ControlVeterinario actualizadoControl = new ControlVeterinario();
                        actualizadoControl.setFecha(String.valueOf(fechaActualizada));
                        actualizadoControl.setRazonVisita(razonActualizado);
                        actualizadoControl.setTratamiento(tratamientoActualizado);
                        actualizadoControl.setCosto(costoActualizado);


                        ControlVeterinarioControllerJDBC controlActualizado = new ControlVeterinarioControllerJDBC(con);
                        boolean resultado3 = controlActualizado.updateAll(idActualizar, fechaActualizada , razonActualizado, tratamientoActualizado, costoActualizado);
                        break;

                    case 7:
                        System.out.println("Ingrese el ID del registro que desea actualizar: ");
                        int idActualizar2 = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Ingrese el nuevo tratamiento: ");
                        String tratamientoActualizado2 = sc.nextLine();

                        ControlVeterinario actualizaTratamiento = new ControlVeterinario();
                        actualizaTratamiento.setTratamiento(tratamientoActualizado2);

                        ControlVeterinarioControllerJDBC actualizacionTratamiento = new ControlVeterinarioControllerJDBC(con);
                        boolean resultado4 = actualizacionTratamiento.updateTratamientoById(idActualizar2,tratamientoActualizado2);
                        break;


                }

        }


    }

    //Primero = Menu para seleccionar la tabla
    public static void mostrarMenu() {
        Scanner sc = new Scanner(System.in);
        System.out.println("******************************************************");
        System.out.println("SELECCIONA LA TABLA CON LA CUAL DESEAS TRABAJAR: ");
        System.out.println("1. Dueños");
        System.out.println("2. Mascotas");
        System.out.println("3. Control Veterinario");
        System.out.println("0. Salir");
        System.out.println("******************************************************");


    }


    public static void menuOperaciones() {
        //Segundo = Menu de Operaciones que debe escoger el usuario
        System.out.println("************************************************************************************");
        System.out.println("SELECCIONA QUE OPERACIÓN DESEAS REALIZAR:  ");
        System.out.println("1. Visualizar todos los datos de la tabla seleccionada:  ");
        System.out.println("2. Visualizar los datos de la tabla de acuerdo a la condición de 'ver por Id'");
        System.out.println("3. Mostrar una funcion de agrupación");
        System.out.println("4. Insertar nuevos datos a la tabla");
        System.out.println("5. Borrar datos por clave primaria de la tabla seleccionada");
        System.out.println("6. Actualizar todos los parámetros de la tabla seleccionada");
        System.out.println("7. Actualizar un parámetros de la tabla seleccionada ");
        System.out.println("8. Salir");
        System.out.println("************************************************************************************");


    }
}



