package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {

    public String databaseName = "conexionJava";
    public String ip = "127.0.0.1";
    public String port = "3306";


    /*
    public String USER = "admin";
    public String PASS = "admin";
º*/

    public String USER = "root";
    public String PASS = "Piscola2630!";


    public Conexion(String databaseName, String ip, String port, String USER, String PASS) {
        this.databaseName = databaseName;
        this.ip = ip;
        this.port = port;
        this.USER = USER;
        this.PASS = PASS;
    }

    //Constructor vacio
    public Conexion() {

    }


    //metodo que devuelve la conexion

    public Connection getConnection() {
        String url = "jdbc:mysql://" + ip + ":" + port + "/" + this.databaseName + "?serverTimezone=UTC";
        Connection con = null;
        try {
            con = DriverManager.getConnection(url, this.USER, this.PASS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return con;
    }

}
